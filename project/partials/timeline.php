    <div class="timeline-cover"></div>
    <div class="timeline-cover-playhead"></div>

    <!-- Full settings menu with includes for language and cc menus -->
    <?php include_once "partials/settings-menu.php" ?>
    <!-- Quality Popout -->
    <?php include_once "partials/quality-menu.php" ?>

    <div class="col-xs-2 nopad">
        <div class="timeline-control-button-container">
            <div class="col-xs-4 nopad">
                <div id="jsSkipBackButton" class="timeline-control-button timeline-control-button__back" title="Skip back" tabindex="-1"><span class="sr-only translate" data-translate="SkipBackButtonText">Skip back</span></div>
            </div>
            <div class="col-xs-4 nopad">
                <div id="jsPlayPauseButton" class="timeline-control-button timeline-control-button__play-pause play" title="Play/Pause" tabindex="-1"><span id="jsPlayPauseSRText" class="sr-only translate" data-translate="PlayPauseButtonText">Play/Pause</span></div>
            </div>
            <div class="col-xs-4 nopad">
                <div id="jsSkipForwardButton" class="timeline-control-button timeline-control-button__forward" title="Skip forward" tabindex="-1"><span class="sr-only translate" data-translate="SkipForwardButtonText">Skip forward</span></div>
            </div>
        </div>
    </div>

    <div class="col-xs-8 nopad">
        <?php require_once "partials/timeline-chapters.php" ?>
    </div>

    <div class="col-xs-2 nopad">
        <div class="col-xs-4 nopad" id="jsSettingsTimelineItem">

            <!-- Full Settings Menu -->
            <div id="jsSettingsContainer" class="timeline-control-settings-container">
                <div id="jsSettingsButton" class="timeline-control-settings">
                    <span id="jsSettingsButtonSRText" class="sr-only translate" data-translate="SettingsButtonText">Settings</span>
                    <div id="jsSettingsButtonIcon" class="timeline-control-button timeline-control-button__settings no-focus" tabindex="-1"></div>
                    <!-- Settings Menu include is above to avoid bootstrap sizing issue -->
                </div>
            </div>

            <!-- Quality Only Menu -->
            <div id="jsQualitySelectorContainer" class="timeline-control-quality-only-container">
                <div id="jsQualitySelectorButton" title="Select Quality"><span class="sr-only translate" data-translate="QualityButtonText">Select Quality</span>
                    <div id="jsQualitySelectorButtonIcon" class="timeline-control-button timeline-control-button__quality sd" tabindex="-1"></div>
                    <!-- Quality Popout Menu is above -->
                </div>
            </div>
        </div>

        <div class="col-xs-4 nopad" id="jsFullScreenTimelineItem">
            <div id="jsFullScreenButtonContainer" class="timeline-control-button-container">
                <div id="jsFullScreenButton" class="timeline-control-fullscreen" title="Full Screen">
                    <span id="jsFullScreenButtonSRText" class="sr-only translate" data-translate="FullScreenButtonText">Full Screen</span>
                    <div id="jsFullScreenButtonIcon" class="timeline-control-button timeline-control-button__fullscreen" tabindex="-1"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 nopad" id="jsVolumeTimelineItem">
            <div id="jsVolumeButtonContainer" class="timeline-control-volume-container">
                <div id="jsVolumeButton" class="timeline-control-volume" title="Volume">
                    <span id="jsVolumeButtonSRText" class="sr-only translate" data-translate="VolumeButtonText">Volume</span>
                    <div id="jsVolumeButtonIcon" class="timeline-control-button timeline-control-button__volume" tabindex="-1"></div>
                    <div id="jsVolumeButtonPopout" class="timeline-control-volume__popout">
                        <div id="jsVolumeButtonPopoutBg" class="timeline-control-volume__popout-bg"></div>
                        <div id="jsVolumeButtonPopoutBgSelectedVolume" class="timeline-control-volume__popout-bg-selected-volume">
                            <div id="jsVolumePopoutBall" class="timeline-control-volume__popout-ball"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
